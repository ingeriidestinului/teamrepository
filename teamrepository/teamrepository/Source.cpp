#include "../MyDLL/Logging.h"
#include <fstream>
#include <iostream>

int main() {

	std::ofstream of("syslog.log", std::ios::app);
	Logger logger(std::cout);

	logger.log("Started Application..", Logger::Level::Info);

	return 0;
}